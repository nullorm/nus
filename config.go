package main

import (
	"os"
	"time"

	"gitlab.com/nullorm/core/database"
)

type apiOptions struct {
	addr string
}

// Config config store
type Config struct {
	mongo *database.MongoOptions
	api   *apiOptions
}

// DetectEnvironment figure out the current environment and build config
func (config *Config) DetectEnvironment() {
	env := os.Getenv("ENV")
	switch env {
	case "prod":
		fallthrough
	case "production":
		config.mongo = &database.MongoOptions{Server: "mongodb://polru:passpolru@ds133378.mlab.com:33378/nullcorp", DialTimeout: time.Minute}
		config.api = &apiOptions{addr: ":50001"}
	case "dev":
		fallthrough
	case "devel":
		fallthrough
	case "development":
		config.mongo = &database.MongoOptions{Server: "mongolabs://", DialTimeout: time.Second * 30}
		config.api = &apiOptions{addr: ""}
	case "local":
		fallthrough
	default:
		config.mongo = &database.MongoOptions{Server: "localhost", DialTimeout: time.Second * 10}
		config.api = &apiOptions{addr: ":50001"}
	}
}
