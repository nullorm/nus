package api

import (
	"net/http"

	"gitlab.com/nullorm/nus/api/users"
	"gopkg.in/mgo.v2"
)

// New init api
func New(s *mgo.Session) (mux *http.ServeMux, err error) {
	mux = http.NewServeMux()

	userAPI, err := users.New(s, "Users")
	if err != nil {
		return
	}

	clientAPI, err := users.New(s, "Clients")
	if err != nil {
		return
	}

	mux.HandleFunc("sign-up", clientAPI.SignUp)
	mux.HandleFunc("sign-in", clientAPI.SignIn)
	mux.HandleFunc("/users/sign-up", userAPI.SignUp)
	mux.HandleFunc("/users/sign-in", userAPI.SignIn)
	return
}
