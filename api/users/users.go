package users

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"gitlab.com/nullorm/nus/database"
	"gitlab.com/nullorm/nus/token"
	"gitlab.com/nullorm/nus/validator"

	"golang.org/x/crypto/bcrypt"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Resource handlers for users
type Resource struct {
	collection string
	model      *database.User
	session    *mgo.Session
}

func handleError(err error, code int, w http.ResponseWriter) {
	w.WriteHeader(code)
	message, _ := json.Marshal(&Response{Error: err.Error()})
	fmt.Fprintf(w, "%s", message)
	return
}

// SignUp new user
func (res *Resource) SignUp(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	w.Header().Set("Content-Type", "application/json")

	req := &Request{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		handleError(err, http.StatusBadRequest, w)
		return
	}

	u := req.User
	if u.Email == "" || u.Password == "" || !validator.IsEmail(u.Email) {
		handleError(errors.New("Invalid fields"), http.StatusPreconditionFailed, w)
		return
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.MinCost)
	if err != nil {
		handleError(err, http.StatusInternalServerError, w)
		return
	}

	u.Password = string(hashedPassword)

	u.ID = bson.NewObjectId()
	err = s.DB("").C(res.collection).Insert(u)
	if err != nil {
		handleError(err, http.StatusInternalServerError, w)
		return
	}

	tokenString, err := token.CreateToken(u, "keyboard cat")
	if err != nil {
		handleError(err, http.StatusInternalServerError, w)
		return
	}

	message, _ := json.Marshal(&Response{Data: &Data{tokenString}})
	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, "%s", message)
}

// SignIn user
func (res *Resource) SignIn(w http.ResponseWriter, r *http.Request) {
	s := res.session.Copy()
	defer s.Close()

	w.Header().Set("Content-Type", "application/json")

	req := &Request{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		handleError(err, http.StatusBadRequest, w)
		return
	}

	u := req.User
	if u.Email == "" {
		handleError(errors.New("Invalid fields"), http.StatusPreconditionFailed, w)
		return
	}

	usr := &database.User{}
	err = s.DB("").C(res.collection).Find(bson.M{"email": u.Email}).One(&usr)
	if err != nil {
		handleError(err, http.StatusInternalServerError, w)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(u.Password))
	if err != nil {
		handleError(err, http.StatusUnauthorized, w)
		return
	}

	tokenString, err := token.CreateToken(u, "keyboard cat")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		message, _ := json.Marshal(&Response{Error: err.Error()})
		fmt.Fprintf(w, "%s", message)
		return
	}

	message, _ := json.Marshal(&Response{Data: &Data{tokenString}})
	fmt.Fprintf(w, "%s", message)
}

// New initialize user resource with default values
func New(s *mgo.Session, collection string) (res *Resource, err error) {
	index := mgo.Index{
		Background: true,
		DropDups:   true,
		Key:        []string{"email"},
		Sparse:     true,
		Unique:     true,
	}

	session := s.Copy()
	defer session.Close()

	err = s.DB("").C(collection).EnsureIndex(index)
	if err != nil {
		return
	}

	res = &Resource{collection: collection, session: s}
	return
}
