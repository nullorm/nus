package users

import "gitlab.com/nullorm/nus/database"

// Data content of response
type Data struct {
	Token string `json:"token"`
}

// Response struct
type Response struct {
	Error string `json:"error"`
	Data  *Data  `json:"data"`
}

// Request struct
type Request struct {
	User *database.User
}
