package token

import (
	"crypto/rand"
	"time"

	"gitlab.com/nullorm/nus/database"

	"gopkg.in/mgo.v2/bson"

	jwt "github.com/dgrijalva/jwt-go"
)

// AdditionalClaims claims in addition to standard claims
type AdditionalClaims struct {
	*jwt.StandardClaims
	Email string        `json:"email"`
	ID    bson.ObjectId `json:"id"`
	Name  string        `json:"name"`
}

// createAdditionalClaims creates jwt base struct
func createAdditionalClaims() (claims *jwt.StandardClaims, err error) {
	id := make([]byte, 16)
	_, err = rand.Read(id)
	if err != nil {
		return
	}
	claims = &jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
		Id:        string(id),
		IssuedAt:  time.Now().Unix(),
		Issuer:    "nus",
	}
	return
}

// CreateToken create a new jwt token
func CreateToken(u *database.User, key string) (token string, err error) {
	additionalClaims, err := createAdditionalClaims()
	if err != nil {
		return
	}
	claims := &AdditionalClaims{additionalClaims, u.Email, u.ID, u.Name}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	token, err = jwtToken.SignedString([]byte(key))
	return
}
