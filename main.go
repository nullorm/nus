package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/nullorm/core/database"
	"gitlab.com/nullorm/nus/api"
)

func main() {
	config := &Config{}
	config.DetectEnvironment()

	mongoClient := &database.MongoClient{}
	mongoClient.SetOptions(config.mongo)

	s, err := mongoClient.CreateConnection()
	if err != nil {
		fmt.Println("Error creating mongo session:", err)
		panic(err)
	}
	defer s.Close()
	fmt.Println("Mongo Session pool created.")

	api, err := api.New(s)
	if err != nil {
		panic(err)
	}
	log.Fatal(http.ListenAndServe(config.api.addr, api))
}
