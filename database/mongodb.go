package database

import (
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// MongoOptions options for mongodb
type MongoOptions struct {
	DB          string
	URI         string
	DialTimeout time.Duration
}

// MongoClient mongodb session struct
type MongoClient struct {
	session *mgo.Session
	options *MongoOptions
}

// User data access struct for users
type User struct {
	Email    string
	ID       bson.ObjectId `bson:"_id,omitempty" json:"id"`
	Name     string
	Password string
}

// CreateConnection open a new mongodb session
func (client *MongoClient) CreateConnection() (session *mgo.Session, err error) {
	session, err = mgo.DialWithTimeout(client.options.URI, client.options.DialTimeout)
	client.session = session
	return
}

// SetOptions set option for MongoDb client
func (client *MongoClient) SetOptions(options *MongoOptions) {
	client.options = options
}
